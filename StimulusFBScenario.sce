# Eventcodes:
# 
# 1  - correct answer, contingent feedback
# 2  - wrong answer,   contingent feedback
# 11 - correct answer, noncontingent feedback
# 12 - wrong answer,   noncontingent feedback
#
# 101 - Buttonset 1 button 1
# 102 - Buttonset 1 button 2
# 201 - Buttonset 2 button 1
# 202 - Buttonset 2 button 2



scenario = "StimulusFB";

response_matching = simple_matching;
active_buttons = 4;
write_codes = false;
pulse_width = 50;
default_font = "Arial";
button_codes = 101,102,201,202;

begin;

array {
	LOOP $i 10; $nr = '$i+1';
		trial{
			trial_type = first_response;
			trial_duration = forever;
			picture {bitmap {filename = "Instruktionen/Folie$nr.png"; }; x = 0; y = 0;};
			duration = response;
		};		
	ENDLOOP;
} Instructions;


picture { text {caption="+"; font="Segoe UI"; font_size=50; font_color=125,108,2;  }; x = 0; y = 0; } FixDunkel;
picture { text {caption="+"; font="Segoe UI"; font_size=50; font_color=255,233,127; }; x = 0; y = 0; } FixHell;

array{
	picture { bitmap { filename = "p11_1.bmp"; }; x = -250; y = 0; bitmap { filename = "p12_1.bmp"; }; x = 250; y = 0;};
	picture { bitmap { filename = "p15_1.bmp"; }; x = -250; y = 0; bitmap { filename = "p14_1.bmp"; }; x = 250; y = 0;};
	picture { bitmap { filename = "p1_1.bmp"; }; x = -250; y = 0; bitmap { filename = "p2_1.bmp"; }; x = 250; y = 0;};
	picture { bitmap { filename = "p3_1.bmp"; }; x = -250; y = 0; bitmap { filename = "p4_1.bmp"; }; x = 250; y = 0;};
} stim;

array{
	picture { bitmap { filename = "p11_1_ch.bmp"; }; x = -250; y = 0; bitmap { filename = "p12_1.bmp"; }; x = 250; y = 0;};
	picture { bitmap { filename = "p15_1_ch.bmp"; }; x = -250; y = 0; bitmap { filename = "p14_1.bmp"; }; x = 250; y = 0;};
	picture { bitmap { filename = "p1_1_ch.bmp"; }; x = -250; y = 0; bitmap { filename = "p2_1.bmp"; }; x = 250; y = 0;};
	picture { bitmap { filename = "p3_1_ch.bmp"; }; x = -250; y = 0; bitmap { filename = "p4_1.bmp"; }; x = 250; y = 0;};
} stimL;

array{
	picture { bitmap { filename = "p11_1.bmp"; }; x = -250; y = 0; bitmap { filename = "p12_1_ch.bmp"; }; x = 250; y = 0;};
	picture { bitmap { filename = "p15_1.bmp"; }; x = -250; y = 0; bitmap { filename = "p14_1_ch.bmp"; }; x = 250; y = 0;};
	picture { bitmap { filename = "p1_1.bmp"; }; x = -250; y = 0; bitmap { filename = "p2_1_ch.bmp"; }; x = 250; y = 0;};
	picture { bitmap { filename = "p3_1.bmp"; }; x = -250; y = 0; bitmap { filename = "p4_1_ch.bmp"; }; x = 250; y = 0;};
} stimR;


array{
	trial{
		trial_type = fixed;
		stimulus_event{ 
			picture FixHell; 
			time = 0;
			code = "fix_hell";
			duration = next_picture;
		};
		stimulus_event{
			nothing{}; 
			delta_time= 1000;
			code = "Stim";
			duration = next_picture;
		};
		stimulus_event{
			nothing{}; 
			delta_time= 1000;
			code = "Stim";
			duration = 500;
		};	
	} StimTrialL;

	trial{
		trial_type = fixed;
		stimulus_event{ 
			picture FixHell; 
			time = 0;
			code = " ";
			duration = next_picture;
		};
		stimulus_event{
			nothing{}; 
			delta_time= 1000;
			code = "Stim";
			duration = next_picture;
		}StimR2;
		stimulus_event{
			nothing{}; 
			delta_time= 1000;
			code = "Stim";
			duration = 500;
		}StimR3;		
	} StimTrialR;
}StimTrial;


trial{
	trial_type = first_response;
	stimulus_event{ 
		picture FixHell; 
		time = 0;
		code = "fix_hell";
		duration = next_picture;
	};
	stimulus_event{
		nothing{}; 
		delta_time= 1000;
		code = "Stim";
		duration = next_picture;
	} TestStim;
} TestTrial;

trial{
	trial_type = fixed;
	stimulus_event{
		nothing{}; 
		code = "ChosenR";
		duration = 500;
	} ChosenTestStimR;
} TestTrialChosenR;

trial{
	trial_type = fixed;
	stimulus_event{
		nothing{}; 
		duration= 500;
		code = "ChosenL";
	} ChosenTestStimL;
} TestTrialChosenL;


trial{ #lateClick
	stimulus_event{ 
		picture {text {caption=" ";}; x = 0; y = 0;};
		time = 0;
		code = "FixHell";
		duration = next_picture;
	};
	stimulus_event{ 
		picture {text {caption="Bitte schneller reagieren."; font_color=255,233,127; font_size=32; }; x = 0; y = 0;};
		duration = 1000;
		time = 500;
	};
} TestLate;


$stim=EXPARAM("Positiver Stimulus");


array{
	trial{ #Positiv
		stimulus_event{ 
			picture FixHell; 
			time = 0;
			code = "FixHell";
			duration = next_picture;
		};
		stimulus_event{ 
			picture {
				polygon_graphic {
					IF '$stim == 1'; # Dreieck
						sides = 3;
					ENDIF;
					IF '$stim == 2'; # Quadrat
						sides = 4;
						rotation  = 45;
					ENDIF;
					radius = 200;
					line_color = 255,255,255;
					line_width = 10;
					fill_color = 0,0,0;
				}; x = 0; y = 0;
			};
			time=EXPARAM ("Delay": 500);
			duration = 1000;
		};
	} ;

	trial{ #Negativ
		stimulus_event{ 
			picture FixHell; 
			time = 0;
			code = "FixHell";
			duration = next_picture;
		};
		stimulus_event{ 
			picture {
				polygon_graphic {
					IF '$stim == 2'; # Dreieck
						sides = 3;
					ENDIF;
					IF '$stim == 1'; # Quadrat
						sides = 4;
						rotation  = 45;
					ENDIF;
					radius = 200;
					line_color = 255,255,255;
					line_width = 10;
					fill_color = 0,0,0;
				}; x = 0; y = 0;
			};
			time=EXPARAM ("Delay": 500);
			duration = 1000;
		};
	} ;
	trial{ #lateClick
		stimulus_event{ 
			picture {text {caption=" ";}; x = 0; y = 0;};
			time = 0;
			code = "FixHell";
			duration = next_picture;
		};
		stimulus_event{ 
			picture {text {caption="Bitte schneller reagieren."; font_color=255,233,127; font_size=32; }; x = 0; y = 0;};
			duration = 1000;
			time = 500;
		};
	} ;
} Feedback;


	trial{ #Pause
		stimulus_event{ 
			picture FixHell; 
			time = 0;
			code = "FixHell";
			duration = next_picture;
		};
		stimulus_event{ 
			picture {text {caption="Kurze Pause. Weiter mit einem Tastendruck."; font_color=255,233,127; font_size=32; }; x = 0; y = 0;};
			duration = 1000;
			time = 500;
		} ;
	} Pause;

begin_pcl;

	int delay = parameter_manager.get_int("Delay");
	int pos_stim = parameter_manager.get_int("Positiver Stimulus");
	int neg_stim = 3 - pos_stim;
	int set = 1;
	
	array <string> stimname [2] = {"Dreieck", "Quadrat"};

	array <int> results [100];
	results.fill( 1, 70, 1, 0);   #pos
	results.fill( 71, 100, 2, 0); #neg
	results.shuffle();

	string activeProband = parameter_manager.get_string("Beobachteter Proband");
	string passiveProband = parameter_manager.get_string("Anwesender Proband");
	
	string proband  = passiveProband + "_" + activeProband;
	#string proband = parameter_manager.get_string("Proband");
	logfile.set_filename(proband + ".log");

	array <int> jitter [11] = {1500, 1600, 1700, 1800, 1900, 2000, 2100, 2200, 2300, 2400, 2500};

	array <int> uebung [10];
	uebung.fill( 1, 5, 1, 0);   #L
	uebung.fill( 6, 10, 2, 0);  #R
	uebung.shuffle();
	
	output_file behaviour = new output_file();
	behaviour.open(proband + "_behaviour.csv");
	behaviour.print_line("version;delay;trial;target;button;feedback;code;rt");
	output_file testbehaviour = new output_file();
	testbehaviour.open(proband + "_testbehaviour.csv");
	testbehaviour.print_line("version;delay;trial;target;button;switched;code;rt");
	
	sub Switch (bool doit) begin
		if (doit) then
			loop int i=1 until i > 4 begin
				stim[i].set_part_x(1, 250);
				stim[i].set_part_x(2, -250);
				stimR[i].set_part_x(1, 250);
				stimR[i].set_part_x(2, -250);
				stimL[i].set_part_x(1, 250);
				stimL[i].set_part_x(2, -250);
				i=i+1;
			end;
		else
			loop int i=1 until i > 4 begin
				stim[i].set_part_x(1, -250);
				stim[i].set_part_x(2, 250);
				stimR[i].set_part_x(1, -250);
				stimR[i].set_part_x(2, 250);
				stimL[i].set_part_x(1, -250);
				stimL[i].set_part_x(2, 250);
				i=i+1;
			end;	
		end;
		#StimTrialR.redraw()
	end;
	
	sub ChooseSet(int i) begin
		StimR2.set_stimulus(stim[i]);
		StimR3.set_stimulus(stimR[i]);
		TestStim.set_stimulus(stim[i]);
		ChosenTestStimR.set_stimulus(stimR[i]);
		ChosenTestStimL.set_stimulus(stimL[i]);
		StimTrialL.get_stimulus_event(2).set_stimulus(stim[i]);
		StimTrialL.get_stimulus_event(3).set_stimulus(stimL[i]);
		
	end;
	
	int target=0;

	sub Test_trial (int i) begin
		int button;
		int feedback= 0;

		bool switched = random(1,2) == 1;
		Switch (switched);
		int jit = jitter[random(1,11)];
		TestTrial.set_duration(jit+3500);
		TestTrial.get_stimulus_event(2).set_delta_time(jit);
		int time = clock.time();
		TestTrial.present();
		int rt = clock.time()-time-jit;
		
		if (rt < 3500) then
			response_data data = response_manager.last_response_data();
			button = data.button();	
			if((button == 1 && !switched) || (button == 2 && switched)) then
				TestTrialChosenL.present();
			else
				TestTrialChosenR.present();
			end;
		else
			TestLate.present();
		end;
		
		int code = 0;
		
		if((button == target && !switched) || (button != target && switched))then
			code = 1;
		else
			code = 2;
		end;
		
		testbehaviour.print(stimname[pos_stim]); testbehaviour.print(";");
		testbehaviour.print(delay);      		  testbehaviour.print(";");
		testbehaviour.print(i);        			  testbehaviour.print(";");
		testbehaviour.print(target);   			  testbehaviour.print(";");
		testbehaviour.print(button);   			  testbehaviour.print(";");
		testbehaviour.print(switched); 			  testbehaviour.print(";");
		testbehaviour.print(code);     			  testbehaviour.print(";");
		testbehaviour.print(rt);       			  testbehaviour.print_line(" ");
	end;
	
	sub Test_trialUebung (int i) begin
		int button;
		bool switched = random(1,2) == 1;
		Switch (switched);
		int jit = jitter[random(1,11)];
		TestTrial.set_duration(jit+3500);
		TestTrial.get_stimulus_event(2).set_delta_time(jit);
		int time = clock.time();
		TestTrial.present();
		int rt = clock.time()-time-jit;
		
		if (rt < 3500) then
			response_data data = response_manager.last_response_data();
			button = data.button();	
			if((button == 1 && !switched) || (button == 2 && switched)) then
				TestTrialChosenL.present();
			else
				TestTrialChosenR.present();
			end;
		else
			TestLate.present();
		end;
	

	end;
	
	input_file input = new input_file();
	input.open("Logs\\" + activeProband + "_behaviour.csv");
	string header = input.get_line();
	term.print_line(header);
	
	
	sub Main_trial (int i) begin
		string line = input.get_line();
		if (line == "") then return; end;
		array<string> parts[8];
		line.split(";", parts);
		int rt = int(parts[8]);	
		int button = int(parts[5]);
		int feedback = int(parts[6]);
		target = int(parts[4]);
		
		bool switched = random(1,2) == 1;
		Switch (switched);
		if(rt < 0)  then 
			rt = 1;
		end;
		
		if((button == 1 && !switched) || (button == 2 && switched)) then
			StimTrialL.get_stimulus_event(3).set_delta_time(rt);
			StimTrialL.present();
		end;
		if((button == 1 && switched) || (button == 2 && !switched)) then
			StimTrialR.get_stimulus_event(3).set_delta_time(rt);
			StimTrialR.present();
		end;
		if (button == 3) then
			button = random(1,2);
			bool rand7030 = random(1,10) > 3;
			if (button == target) then
				if(rand7030) then
					feedback = pos_stim;
				else
					feedback = neg_stim;
				end;
			else
				if(rand7030) then
					feedback = neg_stim;
				else
					feedback = pos_stim;
				end;
			end;
		end;
		
		int code = int(button == target);
		
		Feedback[feedback].get_stimulus_event(2).set_port_code(code);
		Feedback[feedback].get_stimulus_event(2).set_event_code(string(code));
		
		Feedback[feedback].present();
		
		behaviour.print(stimname[pos_stim]); behaviour.print(";");
		behaviour.print(delay);    behaviour.print(";");
		behaviour.print(i);        behaviour.print(";");
		behaviour.print(target);   behaviour.print(";");
		behaviour.print(button);   behaviour.print(";");
		behaviour.print(feedback); behaviour.print(";");
		behaviour.print(code);     behaviour.print(";");
		behaviour.print(rt);       behaviour.print_line(" ");
	end;

	if (delay < 3000) then # short
		ChooseSet(1);
	else # long
		ChooseSet(2);		
	end;


	Instructions[1].present();
	if(pos_stim == 1) then 
		Instructions[2].present();
	end;
	if(pos_stim == 2) then 
		Instructions[3].present();
	end;
	Instructions[4].present();
	
	loop int i=1 until i > uebung.count() begin
		StimTrial[uebung[i]].get_stimulus_event(2).set_delta_time(jitter[random(1,11)]);
		StimTrial[uebung[i]].present();
		int feedback = random(1,2);
		Feedback[feedback].present();
		i = i + 1;
	end;
	
	Instructions[10].present();
	
	loop int i=1 until i > uebung.count() begin
		Test_trialUebung(i);
		i = i + 1;
	end;
	
	Instructions[5].present();
	loop int i=1 until i > results.count() / 2 begin
		Main_trial(i);
		i = i + 1;
	end;
	Instructions[6].present();
	loop int j=1 until j > 20 begin
		Test_trial(j);
		j = j + 1;
	end;	
	
	if (delay < 3000) then # short
		ChooseSet(3);
	else # long
		ChooseSet(4);		
	end;

	Instructions[8].present();
	loop int i=(results.count() / 2) + 1 until i > results.count() begin
		Main_trial(i);
		i = i + 1;
	end;
	Instructions[6].present();
	loop int j=1 until j > 20 begin
		Test_trial(j);
		j = j + 1;
	end;	

	Instructions[9].present();
	behaviour.close();
	testbehaviour.close();
	input.close();